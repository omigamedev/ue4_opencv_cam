

#pragma once

#include "GameFramework/Actor.h"
#include "VideoSurface.generated.h"

/**
 * 
 */
UCLASS()
class AVideoSurface : public AActor
{
    GENERATED_UCLASS_BODY()

    UPROPERTY(EditAnywhere, Category = VideoSurface)
    UStaticMesh* Mesh;
    
    UPROPERTY(EditAnywhere, Category = VideoSurface)
    UMaterial* BaseMaterial;

    TSubobjectPtr<UStaticMeshComponent> MeshComp;
    UMaterialInstanceDynamic* DynMat;
    UTexture2D* Texture;

    virtual void Tick(float DeltaSeconds);
    virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent);
    virtual ~AVideoSurface();
};
