

#include "UE4_OpenCV_Cam.h"
#include "UE4_OpenCV_CamGameMode.h"
#include "UE4_OpenCV_CamPlayerController.h"

AUE4_OpenCV_CamGameMode::AUE4_OpenCV_CamGameMode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	PlayerControllerClass = AUE4_OpenCV_CamPlayerController::StaticClass();
}


