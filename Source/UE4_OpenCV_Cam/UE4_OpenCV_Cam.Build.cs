

using UnrealBuildTool;
using System.IO;

public class UE4_OpenCV_Cam : ModuleRules
{
    private string ModulePath
    {
        get { return Path.GetDirectoryName(RulesCompiler.GetModuleFilename(this.GetType().Name)); }
    }

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "../../ThirdParty/")); }
    }

    public UE4_OpenCV_Cam(TargetInfo Target)
    {
        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
        PrivateDependencyModuleNames.AddRange(new string[] {  });

        PublicSystemIncludePaths.Add(ThirdPartyPath + "opencv/include");
        string libs = ThirdPartyPath + "opencv/lib/";

        PublicAdditionalLibraries.Add(libs + "opencv_core248.lib");
        PublicAdditionalLibraries.Add(libs + "opencv_highgui248.lib");
        PublicAdditionalLibraries.Add(libs + "opencv_imgproc248.lib");
    }
}
