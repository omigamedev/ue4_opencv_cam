

#include "UE4_OpenCV_Cam.h"

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "AllowWindowsPlatformTypes.h"
//#define _CRT_SECURE_NO_WARNINGS
#include <atomic>
#include <thread>
#include <mutex>
#include "HideWindowsPlatformTypes.h"

#include "VideoSurface.h"

std::once_flag flag;
std::atomic<bool> running;
cv::Mat frame(512, 512, CV_8UC3);

void cam_loop()
{
    cv::Mat capture;
    cv::VideoCapture cam(0);
    if (cam.isOpened())
    {
        double dWidth = cam.get(CV_CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
        double dHeight = cam.get(CV_CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

        UE_LOG(LogClass, Log, TEXT("CAMERA READY %dx%d"), (int)dWidth, (int)dHeight);

        running = true;
        while (running)
        {
            if (cam.read(capture))
            {
                cv::resize(capture, frame, cv::Size(512, 512));
            }
        }
    }
    else
    {
        UE_LOG(LogClass, Log, TEXT("CAMERA NOT READY"));
    }
    cam.release();
}

AVideoSurface::AVideoSurface(const class FPostConstructInitializeProperties& PCIP)
    : Super(PCIP)
{
    static ConstructorHelpers::FObjectFinder<UStaticMesh> 
        _defaultMesh(TEXT("StaticMesh'/Engine/EditorMeshes/EditorPlane.EditorPlane'"));
    static ConstructorHelpers::FObjectFinder<UMaterial> 
        _defaultMaterial(TEXT("Material'/Game/SurfaceMaterial.SurfaceMaterial'"));
    static ConstructorHelpers::FObjectFinder<UTexture2D>
        _defaultTexture(TEXT("Texture2D'/Engine/EditorMaterials/WidgetGridVertexColorMaterial.WidgetGridVertexColorMaterial'"));

    Mesh = _defaultMesh.Object;
    MeshComp = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("MeshComponent"));
    MeshComp->SetStaticMesh(Mesh);
    RootComponent = MeshComp;

    BaseMaterial = _defaultMaterial.Object;
    Texture = UTexture2D::CreateTransient(512, 512);
    //Texture->LinkStreaming();
    DynMat = UMaterialInstanceDynamic::Create(BaseMaterial, this);
    //DynMat->SetTextureParameterValue(FName("Texture"), _defaultTexture.Object);
    MeshComp->SetMaterial(0, DynMat);

    PrimaryActorTick.bCanEverTick = true;

    std::call_once(flag, []{std::thread t(cam_loop); t.detach(); });
}

void AVideoSurface::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    if (running)
    {
        FColor* TextureData = (FColor*)Texture->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
        int pixels = Texture->GetSizeX() * Texture->GetSizeY();
        FColor colors[10];
        cv::Vec3b* src = (cv::Vec3b*)frame.data;
        for (int i = 0; i < pixels; i++)
        {
            TextureData[i].B = src[i][0];
            TextureData[i].G = src[i][1];
            TextureData[i].R = src[i][2];
        }
        Texture->PlatformData->Mips[0].BulkData.Unlock();
        Texture->UpdateResource();
        DynMat->SetTextureParameterValue(FName("Texture"), Texture);
        MeshComp->SetMaterial(0, DynMat);
    }

}

void AVideoSurface::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
    if (GET_MEMBER_NAME_CHECKED(AVideoSurface, Mesh) == PropertyChangedEvent.MemberProperty->GetFName())
    {
        MeshComp->SetStaticMesh(Mesh);
    }
}

AVideoSurface::~AVideoSurface()
{
    running = false;
}


