﻿// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class UEOpenCV : ModuleRules
{
    public UEOpenCV(TargetInfo Target)
    {
        Type = ModuleType.External;

        string Path = UEBuildConfiguration.UEThirdPartyDirectory + "opencv/";

        PublicSystemIncludePaths.Add(Path + "include");

        string LibPath = Path + "lib/";

        if (Target.Platform == UnrealTargetPlatform.Win64)
        {
            LibPath += "Win64/VS" + WindowsPlatform.GetVisualStudioCompilerVersionName();
            PublicLibraryPaths.Add( LibPath );

            //PublicAdditionalLibraries.Add("opencv_calib3d248.lib");
            //PublicAdditionalLibraries.Add("opencv_contrib248.lib");
            PublicAdditionalLibraries.Add("opencv_core248.lib");
            //PublicAdditionalLibraries.Add("opencv_features2d248.lib");
            //PublicAdditionalLibraries.Add("opencv_flann248.lib");
            //PublicAdditionalLibraries.Add("opencv_gpu248.lib");
            PublicAdditionalLibraries.Add("opencv_highgui248.lib");
            PublicAdditionalLibraries.Add("opencv_imgproc248.lib");
            //PublicAdditionalLibraries.Add("opencv_legacy248.lib");
            //PublicAdditionalLibraries.Add("opencv_ml248.lib");
            //PublicAdditionalLibraries.Add("opencv_nonfree248.lib");
            //PublicAdditionalLibraries.Add("opencv_objdetect248.lib");
            //PublicAdditionalLibraries.Add("opencv_ocl248.lib");
            //PublicAdditionalLibraries.Add("opencv_photo248.lib");
            //PublicAdditionalLibraries.Add("opencv_stitching248.lib");
            //PublicAdditionalLibraries.Add("opencv_superres248.lib");
            //PublicAdditionalLibraries.Add("opencv_ts248.lib");
            //PublicAdditionalLibraries.Add("opencv_video248.lib");
            //PublicAdditionalLibraries.Add("opencv_videostab248.lib");

            //PublicDelayLoadDLLs.Add("opencv_calib3d248.dll");
            //PublicDelayLoadDLLs.Add("opencv_contrib248.dll");
            PublicDelayLoadDLLs.Add("opencv_core248.dll");
            //PublicDelayLoadDLLs.Add("opencv_features2d248.dll");
            //PublicDelayLoadDLLs.Add("opencv_flann248.dll");
            //PublicDelayLoadDLLs.Add("opencv_gpu248.dll");
            PublicDelayLoadDLLs.Add("opencv_highgui248.dll");
            PublicDelayLoadDLLs.Add("opencv_imgproc248.dll");
            //PublicDelayLoadDLLs.Add("opencv_legacy248.dll");
            //PublicDelayLoadDLLs.Add("opencv_ml248.dll");
            //PublicDelayLoadDLLs.Add("opencv_nonfree248.dll");
            //PublicDelayLoadDLLs.Add("opencv_objdetect248.dll");
            //PublicDelayLoadDLLs.Add("opencv_ocl248.dll");
            //PublicDelayLoadDLLs.Add("opencv_photo248.dll");
            //PublicDelayLoadDLLs.Add("opencv_stitching248.dll");
            //PublicDelayLoadDLLs.Add("opencv_superres248.dll");
            //PublicDelayLoadDLLs.Add("opencv_ts248.dll");
            //PublicDelayLoadDLLs.Add("opencv_video248.dll");
            //PublicDelayLoadDLLs.Add("opencv_videostab248.dll");
        }
    }
}

